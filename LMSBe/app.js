const express = require('express');
const mongoose = require('mongoose');
const nodemailer = require('nodemailer');
const morgan = require('morgan');
// const ClassesModel = require('./models/Class')
var path = require('path');
const port = process.env.PORT || 3002;
const app = express();


const initRouter = require('./routers/router')

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  next();
});

app.use(morgan('dev'));

// app.use('/api/Content/file/contentUploads', express.static('contentUploads'))
// app.use('/api/submission-exercise/file/studentSubmissionUploads', express.static('studentSubmissionUploads')); //one url
//allurl:  app.use(express.static(path.join(__dirname, 'studentSubmissionUploads')))

// app.use('/api/course/courseImageUploads', express.static('courseImageUploads'));
// app.use('/api/user/uploads', express.static('uploads'));
// app.use('/api/kk', express.static('uploads'));

app.use(express.json())
app.use(express.urlencoded({extended: false}))

const dbHost = process.env.DB_HOST || 'localhost'
const dbPort = process.env.DB_PORT || 27017
const dbName = process.env.DB_NAME || 'my_db_name'
// const uri = process.env.MONGODB_URI || 'mongodb+srv://test:test123@nodetestdb.k1wgb.mongodb.net/lmsdb?retryWrites=true&w=majority'

const uri = process.env.MONGODB_URI || `mongodb://${dbHost}:${dbPort}/${dbName}`

const connectWithRetry = function () { // when using with docker, at the time we up containers. Mongodb take few seconds to starting, during that time NodeJS server will try to connect MongoDB until success.
  return mongoose.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true}, (err) => {
    if (err) {
      console.error('Failed to connect to mongo on startup - retrying in 5 sec', err)
      setTimeout(connectWithRetry, 5000)
    }
  })
}
// mongoose.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true })
connectWithRetry();

app.get('/api/test', async (req,res) => {
  try{
    // const test = await ClassesModel.find({
    //   name: 'abc'
    // }).populate({path: 'students'})
    res.json('Hello');
  }
  catch(err){
    res.status(500).json(err)
  }
});


initRouter(app)

app.listen(port, ()=>{ console.log(`listen on ${port}`)})



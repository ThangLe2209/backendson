const Users = require('./../models/user')
const SHA256 = require('crypto-js/sha256')
const refreshToken = require('./../models/refreshToken')
const respondDto = require('../Dtos/response.dto')
const pageDataDto = require('../Dtos/pageData.dto')
const userReadDto = require('../Dtos/User/userRead.dto')
const jwtService = require('./../services/jwtService')

const getUsers = async (req, res) => {
    Users.find({ roleId: { $ne: 1 } }, (err, users) => {
        if (err) {
            res.status(500).json(err)
        }
        else {
            res.json(users)
        }
    })
}

const getUsersByPage = async (req, res) => {
    const { page, limit, searchValue } = req.query
    const regex = new RegExp(searchValue, 'i');
    const _page = page ? parseInt(page) : 0;
    const _limit = limit ? parseInt(limit) : 10;
    try {
        const usersCount = await Users.find({ $or: [{ username: { $regex: regex } }, { email: { $regex: regex } }] }).countDocuments();
        const userdata = await Users.find({ $or: [{ username: { $regex: regex } }, { email: { $regex: regex } }] }).sort({ roleId: 1 }).skip(_page * _limit).limit(_limit);
        // console.log(userdata);
        // console.log(page);
        // console.log(limit);
        return res.json(respondDto(200, "success", new pageDataDto(usersCount, userdata)))
    }
    catch (err) {
        return res.status(500).json(err)
    }
}

const getUsersByID = async (req, res) => {
    try {
        const userdata = await Users.findById({ _id: req.params.id });
        return res.json(respondDto(200, "success", new userReadDto(userdata)));
    }
    catch (err) {
        return res.status(500).json(err)
    }
}

const changeRole = async (req, res) => {
    try{
        const userCount = await Users.findById({ _id: req.params.id }).countDocuments();
        //console.log(userCount);
        if (userCount === 0) return res.status(401).json(respondDto(401, "User isn't exist.", ""));
        const updateRole = await Users.findOneAndUpdate({ _id: req.params.id }, { roleId: req.query.roleId });
        return res.json(respondDto(202, "success", null));
    }
    catch (err) {
        return res.status(500).json(err)
    }
}


const changeAvatar = async (req, res) => {
    try{
    const userCount = await Users.find({ username: req.query.username}).countDocuments();
    if (userCount === 0) return res.status(401).json(respondDto(401, "User isn't exist.", null));
    const userUpdate = await Users.updateOne({username: req.query.username}, {image: req.file.path})
    return res.json(respondDto(202, "success", null));
    }
    catch (err) {
        return res.status(500).json(err)
    }
}

const getUserAvatarByID = async (req, res) => {
    try{
        const user = await Users.find({ image: req.params.image });
        if (!user) return res.status(400).json(respondDto(400, "User isn't exist.", null));
        if(user.image === null) return res.status(400).json(respondDto(400, "User has not avatar", null));
    }
    catch (err) {
        return res.status(500).json(err)
    }
}

const getUsersByToken = async (req, res) => {
    try{

        const decoded = await jwtService.verifyToken(req.params.token, accessTokenSecret)
        const userId = decoded.userId
        const userdata = await Users.findById({ _id: userId})
        if(!userdata) res.status(401).json(`User isn't exist.`);
        return respondDto(200, "success", new userReadDto(userdata))
    }
    catch (err) {
        return res.status(500).json(err)
    }
}



module.exports = {
    getUsers,
    getUsersByPage,
    getUsersByID,
    changeRole,
    changeAvatar,
    getUserAvatarByID,
    getUsersByToken
}
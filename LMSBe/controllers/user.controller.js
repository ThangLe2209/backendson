const Users = require('./../models/user')
const SHA256 = require('crypto-js/sha256')
const refreshToken = require('./../models/refreshToken')
const respondDto = require('../Dtos/response.dto')
const pageDataDto = require('../Dtos/pageData.dto')
const userReadDto = require('../Dtos/User/userRead.dto')
const jwtService = require('./../services/jwtService')
const fs = require("fs")
const { uploadFile } = require('../services/s3')

const accessTokenSecret = process.env.ACCESS_TOKEN_SECRET

const getUsers = async (req, res) => {
    const { page, limit, searchValue } = req.query
    const regex = new RegExp(searchValue, 'i');
    const _page = page ? parseInt(page) : 0;
    const _limit = limit ? parseInt(limit) : 10;
    try {
        const usersCount = await Users.find({ $and: [{ username: { $regex: regex } }, { _id: { $ne: req.userData.UserId } }] }).countDocuments();
        const userdata = await Users.find({ $and: [{ username: { $regex: regex } }, { _id: { $ne: req.userData.UserId } }] }).sort({ roleId: 1 }).skip(_page * _limit).limit(_limit);

        const response = userdata.map(data => { return userReadDto(data) })
        // console.log(response)
        return res.status(200).json(respondDto(200, "success", pageDataDto(usersCount, response)))
    }
    catch (err) {
        return res.status(500).json(err)
    }
}

const getUsersByPage = async (req, res) => {
    const { page, limit, searchValue } = req.query
    const regex = new RegExp(searchValue, 'i');
    const _page = page ? parseInt(page) : 0;
    const _limit = limit ? parseInt(limit) : 10;
    try {
        const usersCount = await Users.find({ $or: [{ username: { $regex: regex } }, { email: { $regex: regex } }] }).countDocuments();
        const userdata = await Users.find({ $or: [{ username: { $regex: regex } }, { email: { $regex: regex } }] }).sort({ roleId: 1 }).skip(_page * _limit).limit(_limit);
        // console.log(userdata);
        // console.log(page);
        // console.log(limit);
        return res.status(200).json(respondDto(200, "success", pageDataDto(usersCount, userdata)))
    }
    catch (err) {
        return res.status(500).json(err)
    }
}

const getUsersByID = async (req, res) => {
    try {
        const userdata = await Users.findById({ _id: req.params.id });
        return res.status(200).json(respondDto(200, "success", userReadDto(userdata)));
    }
    catch (err) {
        return res.status(500).json(err)
    }
}

const changeRole = async (req, res) => {
    try {
        const userCount = await Users.findById({ _id: req.params.id }).countDocuments();
        //console.log(userCount);
        if (userCount === 0) return res.status(401).json(respondDto(401, "User isn't exist.", ""));
        const updateRole = await Users.findOneAndUpdate({ _id: req.params.id }, { roleId: req.query.roleId });
        return res.status(202).json(respondDto(202, "success", null));
    }
    catch (err) {
        return res.status(500).json(err)
    }
}


const changeAvatar = async (req, res) => {
    try {
        const token = req.headers.authorization.split(' ')[1]
        if (!token) return res.status(401).json(`Do not have token`);
        const decoded = await jwtService.verifyToken(token, accessTokenSecret)
        // console.log(decoded)
        const userId = decoded.UserId
        const userName = decoded.Name
        const userData = await Users.findOne({ username: userName })
        if (!userData) return res.status(401).json(respondDto(401, "User isn't exist.", null));
        // if(userData.image) {
        //     const pathToFile = userData.image
        //     fs.unlinkSync(pathToFile)
        // }
        // console.log(result)
        // console.log(req.file)
        const userUpdate = await Users.updateOne({ username: userName }, { image: req.file.filename })
        const result = await uploadFile(req.file)
        const pathToFile = req.file.path
        fs.unlinkSync(pathToFile)
        return res.status(200).json(respondDto(200, "success", null));
    }
    catch (err) {
        return res.status(500).json(err)
    }
}

const getUserAvatarByID = async (req, res) => {
    try {
        const user = await Users.find({ image: req.params.image });
        if (!user) return res.status(400).json(respondDto(400, "User isn't exist.", null));
        if (user.image === null) return res.status(400).json(respondDto(400, "User has not avatar", null));
        res.redirect(`http://localhost:3002/api/user/user.image`);
    }
    catch (err) {
        return res.status(500).json(err)
    }
}

const getUsersByToken = async (req, res) => {
    try {
        const token = req.headers.authorization.split(' ')[1]
        if (!token) return res.status(401).json(`Do not have token`);
        const decoded = await jwtService.verifyToken(token, accessTokenSecret)
        // console.log(decoded)
        const userId = decoded.UserId
        const userName = decoded.Name
        const userdata = await Users.findById({ _id: userId })
        if (!userdata) return res.status(401).json(`User isn't exist.`);
        // console.log(userdata._id);
        return res.status(200).json(respondDto(200, "success", userReadDto(userdata)));
    }
    catch (err) {
        return res.status(500).json(err)
    }
}

const changeInfo = async (req, res) => {
    try {
        const token = req.headers.authorization.split(' ')[1]
        if (!token) return res.status(401).json(`Do not have token`);
        const decoded = await jwtService.verifyToken(token, accessTokenSecret)
        const userId = decoded.UserId
        const userdata = await Users.findById({ _id: userId })
        if (!userdata) return res.status(401).json(`User isn't exist.`);
        const updateRole = await Users.updateOne({ _id: userId }, { "$set": { "gender": req.body.gender, "fullName": req.body.fullName, "phone": req.body.phone } });
        return res.status(200).json(respondDto(200, "success", userReadDto(userdata)));
    }
    catch (err) {
        return res.status(500).json(err)
    }
}

const changePassword = async (req, res) => {
    try {
        const token = req.headers.authorization.split(' ')[1]
        if (!token) return res.status(401).json(`Do not have token`);
        const decoded = await jwtService.verifyToken(token, accessTokenSecret)
        const userId = decoded.UserId
        const userdata = await Users.findById({ _id: userId })
        console.log(userdata)
        if (!userdata) return res.status(401).json(`User isn't exist.`);
        if (userdata.password !== req.body.oldpass) return res.status(400).json(respondDto(400, "Wrong old password.", null));
        if (userdata.password === req.body.newpass) return res.status(400).json(respondDto(400, "The new password must be different from the old password.", null));
        if (req.body.confirmpass !== req.body.newpass) return res.status(400).json(respondDto(400, "The new password and confirm password are not match", null));
        // console.log(req.body.oldpass);
        // console.log(req.body.newpass);
        // console.log(req.body.confirmpass);
        const updateUserPassword = await Users.updateOne({ _id: userId }, { "password": req.body.newpass });
        return res.status(200).json(respondDto(200, "success", null));

    }
    catch (err) {
        return res.status(500).json(err)
    }
}

const changeLock = async (req, res) => {
    try {
        const userData = await Users.findById({ _id: req.params.id })
        if (!userData) return res.status(404).json(respondDto(404, "User doesn't exists", null));
        if (userData.isLock === false) {
            const updateUserAccount = await Users.updateOne({ _id: req.params.id }, { $set: { "isLock": true } });
            return res.status(202).json(respondDto(202, "User is locked.", null));
        }
        const updateUserAccount = await Users.updateOne({ _id: req.params.id }, { $set: { "isLock": false } });
        return res.status(202).json(respondDto(202, "User is unlocked.", null));
    }
    catch (err) {
        return res.status(500).json(err)
    }
}


module.exports = {
    getUsers,
    getUsersByPage,
    getUsersByID,
    changeRole,
    changeAvatar,
    getUserAvatarByID,
    getUsersByToken,
    changeInfo,
    changePassword,
    changeLock
}
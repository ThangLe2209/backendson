require('dotenv').config()
const Users = require('../models/user')
const respondDto = require('../Dtos/response.dto')
const jwtService = require('./../services/jwtService')
const SHA256 = require('crypto-js/sha256')
const RefreshToken = require('./../models/refreshToken')
const nodemailer = require('nodemailer');
const mailContent = require('./../services/MailService/MailHtmlForm')

const accessTokenSecret = process.env.ACCESS_TOKEN_SECRET
const verifyTokenSecret = process.env.VERIFY_TOKEN_SECRET
const refreshTokenSecret = process.env.REFRESH_TOKEN_SECRET
const accessTokenLife = process.env.ACCESS_TOKEN_LIFE
const refreshTokenLife = process.env.REFRESH_TOKEN_LIFE
const verifyTokenLife = process.env.VERIFY_TOKEN_LIFE

const login = async (req, res) => {
    const username = req.body.username
    const password = req.body.password
    // console.log(username)
    try {
        const user = await Users.findOne({ username: username })
        // console.log(user)
        if (!user) return res.status(400).json(respondDto(400, "invalid username", null));
        // if (user.password != SHA256(password).toString()) return res.status(400).json({ err: 'invalid password' })
        if (user.password !== password) return res.status(400).json(respondDto(400, "invalid password", null));
        if (user.isLock === true) return res.status(400).json(respondDto(400, "User is locked", null));

        let roleName = '';
        if (user.roleId === 1) {
            roleName = 'Admin';
        }
        else if (user.roleId === 2) {
            roleName = 'Teacher';
        }
        else if (user.roleId === 3) {
            roleName = 'Instructor';
        }
        else roleName = 'Student';

        let dataUser = {
            Name: user.username,
            Role: roleName,
            Verify: user.verify,
            UserId: user._id
        }
        // console.log(dataUser)

        const accessToken = await jwtService.generateToken(dataUser, accessTokenSecret, accessTokenLife)
        // const refreshToken = await jwtService.generateToken(dataUser, refreshTokenSecret, refreshTokenLife)

        // const storedRefreshToken = await RefreshToken.findOne({ userId: dataUser.userId }).exec()
        // if (!storedRefreshToken) {
        //     RefreshToken.create({ userId: dataUser.userId, token: refreshToken })
        // } else {
        //     RefreshToken.findOneAndUpdate({ userId: dataUser.userId }, { token: refreshToken }).exec()
        // }

        if (user.roleId === 1) {
            return res.json(respondDto(200, "success", { token: accessToken, role: 'Admin' }));
        }
        else if (user.roleId === 2) {
            return res.json(respondDto(200, "success", { token: accessToken, role: 'Teacher' }));
        }
        else if (user.roleId === 3) {
            return res.json(respondDto(200, "success", { token: accessToken, role: 'Instructor' }));
        }
        else return res.json(respondDto(200, "success", { token: accessToken, role: 'Student' }));

    } catch(err) {
        return res.status(400).json(err)
    }
}

const userMe = async (req, res) => {
    const token = req.params.token;
    // console.log(token);
    if (token === null || token.length === 0) {
        return res.status(400).json(respondDto(400, "required token", "err"));
    }
    try {
        const decoded = await jwtService.verifyToken(token, accessTokenSecret)
        const username = decoded.Name
        const role = decoded.Role
        // if(email === null) return res.status(400).json(respondDto(400, "invalid token", ""));
        // const user = await Users.findOne({ email: email }).exec()
        // if (!user) return res.status(400).json(respondDto(400, "gmail not found", "err"));
        // const verifyUserMail = await Users.findOneAndUpdate({ username: username }, { verify: true });
        // return res.redirect('https://app-k4yhm75fiq-uc.a.run.app/login');
        return res.json({username: username, role: role});
    }
    catch (error) {
        console.log(error);
        return res.status(500).json(error);
    }
}

const refesh = async (req, res) => {
    const userData = { userId: req.userData.userId }
    try {
        const token = await jwtService.generateToken(userData, accessTokenSecret, accessTokenLife)
        return res.json({ accessToken: token })
    } catch (err) {
        return res.status(403).json(err)
    }
}

const logout = async (req, res) => {
    const userData = req.userData
    try {
        await RefreshToken.findOneAndUpdate({ userId: userData.userId }, { userId: 'block_' + userData.userId })
        return res.json({ status: 'ok' })
    } catch (err) {
        return res.status(500).json({ status: 'err', err: err })
    }
}

const register = async (req, res) => {
    const user = {
        username: req.body.username,
        password: req.body.password,
        email: req.body.email,
        gender: req.body.gender,
        fullName: req.body.fullName,
        phone: req.body.phone,
        isLock: false,
        image: null,
        verify: false,
        createDate: new Date(),
        roleId: 4
    }

    try {
        await Users.create(user);
        res.json(respondDto(200, "success", null));
    }
    catch (err) {
        return res.status(500).json(err)
    }
}

// Mail api

const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'techtechlms@gmail.com',
        pass: '0147258369@'
    }
});

const mailOptions = (userEmail, userName, verifyToken) => {
    let mailHtml = mailContent.vetifyForm(userName, "techedu.com", "https://lmsbetech.herokuapp.com" + "/api/auth/verify?token=" + verifyToken, "techtechlms@gmail.com");
    return {
        from: 'techtechlms@gmail.com',
        to: userEmail,
        subject: 'Verify email',
        html: mailHtml
    };
}


const getVerifyMail = async (req, res) => {
    try {
        const user = await Users.findOne({ email: req.query.email }).exec()
        if (!user) return res.status(400).json(respondDto(400, "email not found", "err"));
        if (user.verify === true) return res.status(400).json(respondDto(400, "email already verify", "err"));
        let dataUser = {
            Name: user.username,
            email: user.email
        }

        const verifyToken = await jwtService.generateToken(dataUser, verifyTokenSecret, verifyTokenLife);
        const sendMail = await transporter.sendMail(mailOptions(req.query.email, user.username, verifyToken));
        // console.log('Email sent: ' + info.response);
        console.log(verifyToken);
        return res.json(respondDto(200, "success", ""));
    }
    catch (error) {
        console.log(error);
        return res.status(500).json(error);
    }
}
const verifyMail = async (req, res) => {
    const token = req.query.token;
    if (token === null || token.length === 0) {
        return res.status(400).json(respondDto(400, "required token", "err"));
    }
    try {
        const decoded = await jwtService.verifyToken(token, verifyTokenSecret)
        const username = decoded.Name
        const email = decoded.email
        if(email === null) return res.status(400).json(respondDto(400, "invalid token", ""));
        const user = await Users.findOne({ email: email }).exec()
        if (!user) return res.status(400).json(respondDto(400, "gmail not found", "err"));
        const verifyUserMail = await Users.findOneAndUpdate({ username: username }, { verify: true });
        return res.redirect('https://app-k4yhm75fiq-uc.a.run.app/login');
        // return res.json(respondDto(200, "success", ""));
    }
    catch (error) {
        console.log(error);
        return res.status(500).json(error);
    }
}

module.exports = {
    login: login,
    refresh: refesh,
    logout: logout,
    register: register,
    getVerifyMail: getVerifyMail,
    verifyMail: verifyMail,
    userMe: userMe
}
const route = require('express').Router()
const authMiddle = require('../middlewares/auth.middleware')
const userController = require('../controllers/user.controller')

const multer = require('multer');

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, './uploads/');
  },
  filename: function(req, file, cb) {
    cb(null, Date.now() + file.originalname);
  }
});

const fileFilter = (req, file, cb) => {
  // reject a file
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 5
  },
  fileFilter: fileFilter
});

route.get('/',[authMiddle.verifyToken, authMiddle.admin], userController.getUsersByPage)
route.get('/get-all', [authMiddle.verifyToken, authMiddle.admin], userController.getUsers) // admin.service.ts (admin folder)
route.get('/detail', userController.getUsersByToken)
route.put('/change-role/:id',userController.changeRole)
route.put('/change-avatar',authMiddle.verifyToken,upload.single('image') ,userController.changeAvatar) //Y admin.service.ts (admin folder)
route.put('/change-lock/:id', [authMiddle.verifyToken, authMiddle.admin],userController.changeLock); //N admin.service.ts (admin folder)
route.put('/change-infor', userController.changeInfo) //Y admin.service.ts (admin folder)
route.put('/change-password', userController.changePassword) //Y admin.service.ts (admin folder)
route.get('/avatar/:image',userController.getUserAvatarByID) // account-detail.component.html admin.service.ts (admin folder) - course-review.component.html (course folder)
route.get('/:id',authMiddle.verifyToken ,userController.getUsersByID) //course.service.ts (course folder)



module.exports = route

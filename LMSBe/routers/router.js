const authRouter = require('./auth.router')
const usersRouter = require('./user.router')
module.exports = (app) => {
    app.use('/api/auth', authRouter)
    app.use('/api/user', usersRouter)
}
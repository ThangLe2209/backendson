const route = require('express').Router()
const authMiddle = require('./../middlewares/auth.middleware')
const authController = require('../controllers/auth.controller')

route.post('/login',authController.login)
route.post('/register',authMiddle.verifyRegister,authController.register)
route.post('/getVerify',authController.getVerifyMail)
route.get('/verify',authController.verifyMail)
route.get('/userMe/:token', authController.userMe) //course.service.ts (course folder)
// route.post('/refresh',authMiddle.verifyRefreshToken,authController.refresh)
// route.post('/logout',authController.logout)

module.exports = route
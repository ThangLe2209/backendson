const answerReadDto = (data) => {
    return {
        id: data._id,
        content: data.content,
        isCorrect: data.isCorrect
    }
}

module.exports = answerReadDto;
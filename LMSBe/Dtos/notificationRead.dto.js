const notificationReadDto = (data) => {
    return {
        id: data._id,
        title: data.title,
        content: data.content,
        createDate: data.createDate,
        type: data.type,
        studentsRead: data.studentsRead,
        classId: data.classId
    }
}
module.exports = notificationReadDto;
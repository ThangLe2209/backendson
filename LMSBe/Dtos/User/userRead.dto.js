function checkRole(roleId) {
    if (roleId === 1) return "Admin";
    else if (roleId === 2) return "Teacher";
    else if (roleId === 3) return "Instructor";
    else return "Student"
}

const userReadDto = (userdata) => {
    return {
        id: userdata._id,
        username: userdata.username,
        email: userdata.email,
        isLock: userdata.isLock,
        fullName: userdata.fullName,
        image: userdata.image,
        gender: userdata.gender,
        phone: userdata.phone,
        roleId: userdata.roleId,
        verify: userdata.verify,
        role: {
            id: userdata.roleId,
            roleName: checkRole(userdata.roleId)
        },
        createDate: userdata.createDate
    }
}

module.exports = userReadDto;


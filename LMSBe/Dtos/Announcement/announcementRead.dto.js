const announcementReadDto = (data) => {
    return {
        id: data._id,
        title: data.title,
        content: data.content,
        createDate: data.createDate,
        type: data.type,
        classId: data.classId
    }
}
module.exports = announcementReadDto;
const quizReadDto = (data) => {
    return {
        id: data._id,
        name: data.name,
        description: data.description,
        createDate: data.createDate,
        startDate: data.startDate,
        duration: data.duration,
        classId: data.classId
    }
}

module.exports = quizReadDto;
function checkRole(roleId) {
    if (roleId === 1) return "Admin";
    else if (roleId === 2) return "Teacher";
    else if (roleId === 3) return "Instructor";
    else return "Student"
}

const classReadDto = (userdata, userdata1) => {
    userdata.map(data => {
        return {
        id: data._id,
        teacherId: data.teacherId,
        teacher: {
            id: userdata1._id,
            username: userdata1.username,
            email: userdata1.email,
            isLock: userdata1.isLock,
            fullName: userdata1.fullName,
            image: userdata1.image,
            gender: userdata1.gender,
            phone: userdata1.phone,
            roleId: userdata1.roleId,
            verify: userdata1.verify,
            role: {
                id: userdata1.roleId,
                roleName: checkRole(userdata1.roleId)
            },
            createDate: userdata1.createDate
        },
        classAdmin: {
            id: userdata1._id,
            username: userdata1.username,
            email: userdata1.email,
            isLock: userdata1.isLock,
            fullName: userdata1.fullName,
            image: userdata1.image,
            gender: userdata1.gender,
            phone: userdata1.phone,
            roleId: userdata1.roleId,
            verify: userdata1.verify,
            role: {
                id: userdata1.roleId,
                roleName: checkRole(userdata1.roleId)
            },
            createDate: userdata1.createDate
        },
        assistant: {
            id: userdata1._id,
            username: userdata1.username,
            email: userdata1.email,
            isLock: userdata1.isLock,
            fullName: userdata1.fullName,
            image: userdata1.image,
            gender: userdata1.gender,
            phone: userdata1.phone,
            roleId: userdata1.roleId,
            verify: userdata1.verify,
            role: {
                id: userdata1.roleId,
                roleName: checkRole(userdata1.roleId)
            },
            createDate: userdata1.createDate
        },
        name: data.name,
        description: data.description,
        createDate: data.createDate
    }})
}

module.exports = classReadDto;
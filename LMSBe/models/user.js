const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const userSchema = new Schema({
    id: Number,
    username: String,
    password: String,
    email: String,
    isLock: Boolean,
    gender: String,
    fullName: String,
    image: String,
    phone: {type: String, required: true},
    roleId: Number,
    verify: Boolean,
    createDate: Date
}, { collection: 'users'});
// { collection: 'users', timestamps: {} }

var Users = mongoose.model("users", userSchema);

module.exports = Users;